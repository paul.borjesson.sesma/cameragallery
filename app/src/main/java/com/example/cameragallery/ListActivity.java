package com.example.cameragallery;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    private StorageReference storageReference;
    private RecyclerView recyclerView;
    private MyAdapter myAdapter;
    private ArrayList<String> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        recyclerView = findViewById(R.id.recView);

        storageReference = FirebaseStorage.getInstance().getReference().child("image");
        storageReference.listAll().addOnSuccessListener(listResult -> {
            for (StorageReference fileRef: listResult.getItems()) {
                fileRef.getDownloadUrl()
                        .addOnSuccessListener(uri -> images.add(uri.toString()))
                        .addOnSuccessListener(uri -> recyclerView.setAdapter(myAdapter));
            }
        });
        myAdapter = new MyAdapter(images, this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        private Context context;
        private ArrayList<String> images;

        public MyAdapter(ArrayList<String> images, Context context) {
            this.images = images;
            this.context = context;
        }

        @NonNull
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.row_data, parent, false );
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
            Picasso.get().load(images.get(position))
                    .fit()
                    .centerCrop()
                    .into(holder.imageView);
            holder.imageView.setOnClickListener(view -> {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("imageUrl", images.get(position));
                startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return images.size();
        }
        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView imageView;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.imageRow);
            }
        }
    }

}