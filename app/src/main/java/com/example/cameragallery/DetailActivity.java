package com.example.cameragallery;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailActivity extends AppCompatActivity {

    private ImageView imagen;
    private Button btnDownload;
    private Button btnDel;
    private StorageReference storage = FirebaseStorage.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Hooks
        imagen = findViewById(R.id.imageDetail);
        btnDownload = findViewById(R.id.btnDownload);
        btnDel = findViewById(R.id.btnDelete);

        String imageUrl = getIntent().getStringExtra("imageUrl");

        Picasso.get().load(imageUrl)
                .fit()
                .centerCrop()
                .into(imagen);

        btnDownload.setOnClickListener(view -> downloadFile(DetailActivity.this, createFileName(), ".jpg", String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)), imageUrl));

        btnDel.setOnClickListener(view -> {
            deleteImage(storage.getStorage().getReferenceFromUrl(imageUrl));
            Intent intent = new Intent(this, ListActivity.class);
            startActivity(intent);
            finish();
        });

    }

    public void downloadFile(Context context, String fileName, String fileExt, String destination,
                             String url) {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalFilesDir(context, destination, fileName + fileExt);
        Toast.makeText(getApplicationContext(), "Image downloaded",
                Toast.LENGTH_SHORT).show();
        downloadManager.enqueue(request);
    }

    private String createFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp;
        return imageFileName;
    }

    private void deleteImage(StorageReference httpsReference) {
        httpsReference.delete().addOnSuccessListener(unused -> Toast.makeText(getApplicationContext(), "Image deleted",
                Toast.LENGTH_SHORT).show()).addOnFailureListener(e -> Toast.makeText(getApplicationContext(), "Error deleting image",
                Toast.LENGTH_SHORT).show());
    }

}